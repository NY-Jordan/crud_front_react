import { Add, Delete, Update } from '@mui/icons-material'
import { Box, Button, CircularProgress, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@mui/material'
import React, { useState, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import AddForm from './Components/AddForm'
import AppBarComponent from './Components/AppBarComponent'
import ListingUser from './Components/ListingUser'
import UpdateForm from './Components/UpdateForm'
import { DeleteUserService, GetUsersService } from './Redux/Services/UserService'

export default function Home() {
    const dispacth = useDispatch();
    useMemo(() => GetUsersService(dispacth), [dispacth]);
    const userState = useSelector(state => state.user);
    const users  = userState.users;
    const [openAddForm, setOpenAddForm] = useState(false);
    const handleClickOpen = () => {
      setOpenAddForm(true);
    };


  return (
    <Box>
        <AppBarComponent />
        <Box>
            <Typography align='center' variant='h4' margin={2}>User List</Typography>
        </Box>
        <Box align='left' marginLeft={"12%"}  marginBottom="25px" onClick={() => handleClickOpen()}>
          <Button variant='contained' startIcon={<Add />}>ADD</Button>
        </Box>
        <AddForm  open={openAddForm} setOpen={setOpenAddForm} />
        <Box align='center'>
        <TableContainer  align='center' component={Paper} sx={{ maxWidth : 1000 , display : "flex", alignItems : "center"  }}>
      <Table  aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>First Name</TableCell>
            <TableCell align="right">Last Name</TableCell>
            <TableCell align="right">Phone</TableCell>
            <TableCell align="right">Gender</TableCell>
            <TableCell >Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {users ?  users.map((user, key) => <ListingUser user={user} /> ): <TableRow
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
              </TableCell>
              <TableCell align="right"><CircularProgress   /></TableCell>
              <TableCell align="right"></TableCell>
              <TableCell align="right"></TableCell>
              <TableCell align="right"></TableCell>
            </TableRow>}

        </TableBody>
      </Table>
    </TableContainer>
    </Box>
    </Box>
  )
}
