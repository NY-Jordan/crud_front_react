import { Box, Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, InputLabel, MenuItem, Select, TextField } from '@mui/material'
import React from 'react'
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { CreateUserService } from '../Redux/Services/UserService';

export default function AddForm({open, setOpen}) {
  const [gender, setGender] = React.useState('M');
  const [loader, setLoader] = React.useState(false);
  const dispatch  = useDispatch();
  const handleChangeGender = (e) => {
    setGender(e.target.value)
  }
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const handleClose = () => {
    setOpen(false);
  };
  const submit = (data) => {
      console.log(data);
      setLoader(true)
      const user = {
        'first_name' : data.firstName,
        'last_name' : data.LastName,
        'gender' : data.gender,
        'phone' : data.Phone,
        'password' : data.password,
      }
      console.log(user);
      CreateUserService(dispatch, user, setLoader);

  }

  return (
      <div>
        <Dialog open={open} onClose={handleClose}>
          <DialogTitle>Add User</DialogTitle>
          <form onSubmit={handleSubmit((data)=> submit(data))}>
            <DialogContent>
              <TextField
                autoFocus
                margin="dense"
                id="text"
                label="First Name"
                type="text"
                fullWidth
                variant="standard"
                {...register('firstName', {
                  required : true,
                  
                })}
              />
              <TextField
                autoFocus
                margin="dense"
                id="text"
                label="Last Name"
                type="text"
                fullWidth
                variant="standard"
                {...register('LastName' , {
                  required : true,
                  
                })}
              />
              <TextField
              autoFocus
              margin="dense"
              id="text"
              label="Phone"
              type="text"
              fullWidth
              variant="standard"
              {...register('Phone', {
                required : true,
                
              })}
            />
            <FormControl variant="standard" fullWidth>
              <InputLabel id="demo-simple-select-label">Age</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={gender}
                label="Age"
                onChange={handleChangeGender}
                {...register('gender', {
                  required : true,
                  
                })}
              >
                <MenuItem value={"M"}>M</MenuItem>
                <MenuItem value={'F'}>F</MenuItem>
              </Select>
            </FormControl>
          <TextField
            autoFocus
            margin="dense"
            id="password"
            label="Password"
            type="password"
            fullWidth
            variant="standard"
            {...register('password', {
              required : true,
              
            })}
          />
          <small style={{  color: 'red'}}>
   password need more than 6 characters
</small>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>Cancel</Button>
              <Button type='submit' >{loader ? <CircularProgress /> : "add"}</Button>
            </DialogActions>
          </form>
        </Dialog>
    </div>
  )
}
