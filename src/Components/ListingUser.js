import { Button, Stack, TableCell, TableRow } from '@mui/material'
import React, { useState } from 'react'
import { Delete } from 'react-axios'
import { useDispatch } from 'react-redux'
import { DeleteUserService } from '../Redux/Services/UserService'
import UpdateForm from './UpdateForm'

export default function ListingUser({user}) {
    const dispacth = useDispatch();
    const [openUpdateForm, setOpenUpdateForm] = useState(false);


  return (
      <TableRow
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              
              <TableCell align="right">{user.first_name}</TableCell>
              <TableCell align="right">{user.last_name}</TableCell>
              <TableCell align="right">{user.phone}</TableCell>
              <TableCell align="right">{user.gender}</TableCell>
              <TableCell align="right">
                <Stack direction={'row'} >
                    <Button color='warning' sx={{ marginRight : "10px" }} variant='contained' onClick={() => setOpenUpdateForm(true)}>Update</Button>

                    <Button color='error'   startIcon={<Delete />} variant='contained' onClick={() => DeleteUserService(dispacth,user.id)}>Delete</Button>
                </Stack>
                <UpdateForm   open={openUpdateForm} setOpen={setOpenUpdateForm}  user={user} />
              </TableCell>
            </TableRow>
  )
}
