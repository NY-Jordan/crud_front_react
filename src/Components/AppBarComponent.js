import { AppBar, Box, Toolbar, Typography } from '@mui/material'
import React from 'react'

export default function AppBarComponent() {
  return (
       <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            User Management
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  )
}
