import { userActions } from "../Actions/UserActions";

const initialState = {
    name: "user",
    users : null,
    error : false,
    message : null,
}


const  UserReducer = (state = initialState, action) => {
    switch (action.type) {
    case userActions.GET_USER_SUCCESS :
        return {name : "user", users : action.payload.users , error: false, message : null} 
    case userActions.GET_USER_FAILED :
        return {...state, error: action.payload.error } 
    case userActions.CREATE_USER_SUCCESS :
        return {...state, message: action.payload.message } 
    case userActions.CREATE_USER_FAILED :
        return {...state, error: action.payload.error } 
    case userActions.UPDATE_USER_SUCCESS :
        return {...state, message: action.payload.message } 
    case userActions.UPDATE_USER_FAILED :
        return {...state, error: action.payload.error } 
    case userActions.DELETE_USER_SUCCESS :
        return {...state, message: action.payload.message } 
    case userActions.DELETE_USER_FAILED :
        return {...state, error: action.payload.error } 

    default:
        return {...state};
    }
};

export default UserReducer;