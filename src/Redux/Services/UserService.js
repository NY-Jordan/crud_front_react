import Apiclient from '../../Helpers/ApiClient';
import { CreateUserFailed, CreateUserSuccess, DeleteUserFailed, DeleteUserSuccess, GetUserFailed, GetUserSuccess, LoginFailed, LoginSuccess, RegisterFailed, UpdateUserFailed, UpdateUserSuccess } from '../Actions/UserActions';

export const GetUsersService = async (dispatch) => {
    setTimeout(() => {
        Apiclient().get('/users') 
            .then((response) => {
                const res = response.data;
                    const users  = res.data; 
                    console.log(users);
                    dispatch(GetUserSuccess(users))
            }).catch((e) => { 
                    const error = e.response.data.message;
                    dispatch(GetUserFailed(error))
            })
      }, 2000)
};
 
export const CreateUserService  = (dispatch, user, setLoader) => {
    setTimeout(() => {
        Apiclient().post('/users', user) 
            .then((response) => {
                   setLoader(false);
                    dispatch(CreateUserSuccess('oparation successfully'));
                    window.location.reload();
            }).catch((e) => { 
                   setLoader(false);
                    console.log(e);
                    dispatch(CreateUserFailed('error'))
            })
      }, 2000)
}
export const UpdateUserService  = (dispatch, user, id, setLoader) => {
    setTimeout(() => {
        Apiclient().post('users/update/'+id, user) 
            .then((response) => {
                setLoader(false);
                    dispatch(UpdateUserSuccess('oparation successfully'))
                    window.location.reload();
            }).catch((e) => { 
                    console.log(e);
                    setLoader(false);
                    dispatch(UpdateUserFailed('error'))
            })
      }, 2000)
}

export const DeleteUserService  = (dispatch, id) => {
    setTimeout(() => {
        Apiclient().post('users/delete/'+id) 
            .then((response) => {
                    dispatch(DeleteUserSuccess('oparation successfully'));
                    window.location.reload();
            }).catch((e) => { 

                    console.log(e);
                    dispatch(DeleteUserFailed('error'))
            })
      }, 2000)
}



