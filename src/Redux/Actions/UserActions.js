export const  userActions = {
    GET_USER_SUCCESS : "GET_USER_SUCCESS" ,
    GET_USER_FAILED : "GET_USER_FAILED" ,

    CREATE_USER_SUCCESS : "CREATE_USER_SUCCESS" ,
    CREATE_USER_FAILED : "CREATE_USER_FAILED" ,

    UPDATE_USER_SUCCESS : "UPDATE_USER_SUCCESS" ,
    UPDATE_USER_FAILED : "UPDATE_USER_FAILED" ,

    DELETE_USER_SUCCESS : "DELETE_USER_SUCCESS" ,
    DELETE_USER_FAILED : "DELETE_USER_FAILED" ,
}


export const GetUserSuccess = (users) => ({
        type : userActions.GET_USER_SUCCESS,
        payload : {users: users}
});

export const GetUserFailed = (error) => ({
    type : userActions.GET_USER_SUCCESS,
    payload : {error : error}
});

export const CreateUserFailed = (error) => ({
    type : userActions.CREATE_USER_FAILED,
    payload : {error : error}
});

export const CreateUserSuccess = (message) => ({
    type : userActions.CREATE_USER_SUCCESS,
    payload : {message : message}
});

export const UpdateUserFailed = (error) => ({
    type : userActions.UPDATE_USER_FAILED,
    payload : {error : error}
});

export const UpdateUserSuccess = (message) => ({
    type : userActions.UPDATE_USER_SUCCESS,
    payload : {message : message}
});


export const DeleteUserSuccess = (message) => ({
    type : userActions.DELETE_USER_SUCCESS,
    payload : {message : message}
});


export const DeleteUserFailed = (error) => ({
    type : userActions.DELETE_USER_FAILED,
    payload : {error : error}
});